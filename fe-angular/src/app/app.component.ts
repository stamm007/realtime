import {
    Component,
    ElementRef,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import { RealtimeService } from './realtime.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Subscription } from 'rxjs';

declare const MediaRecorder: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
    VIDEO_REFRESH = 200; // How often we send the video stream (in ms)
    public showVideo = false;
    public showRecord = false;
    public loadRecord = false;
    public link_Record = '';
    public isRecording: boolean;
    private videoElmt: ElementRef;
    private id: string;
    private video: HTMLVideoElement;
    private webSocketSubscription: Subscription;
    private videoRecorder;
    private videoStream: MediaStream;
    private options = {
        mimeType: 'video/webm;codecs=h264',
        facingMode: 'user',
        videoBitsPerSecond: 200000, // 0.2 Mbit/sec.
        frameRate: {
            ideal: 30,
            max: 30,
        },
    };

    private constraints = {
        frameRate: { exact: 30 },
    };

    constructor(
        private route: ActivatedRoute,
        private realtimeService: RealtimeService,
        private router: Router
    ) {}

    @ViewChild('video', { static: false }) set content(content: ElementRef) {
        if (content) {
            // initially setter gets called with undefined
            this.videoElmt = content;
            this.video = this.videoElmt.nativeElement;
        }
    }

    ngOnInit() {
        this.isRecording = false;
        this.route.paramMap.subscribe((params: ParamMap) => {
            this.id = params.get('id');

            if (this.id === undefined || this.id === null) {
                this.id = this.generate_id(24);
                this.router.navigate(['/realtime', this.id]);
            }
        });
    }

    ngOnDestroy() {
        if (this.webSocketSubscription) {
            this.webSocketSubscription.unsubscribe();
        }
    }

    startRecording() {
        this.showRecord = false;
        if (navigator.mediaDevices === undefined) {
            console.error(
                "Couldn't detect any media device. Please contact the support",
                'dismiss',
                {
                    duration: 4000,
                }
            );
        } else {
            this.showVideo = true;

            navigator.mediaDevices
                .getUserMedia({ video: true })
                .then((videoStream) => {
                    this.videoRecorder = new MediaRecorder(
                        videoStream,
                        this.options
                    );
                    this.videoRecorder.video = this.video;
                    this.video.srcObject = videoStream;
                    this.video.play();
                    this.videoStream = videoStream;

                    this.videoRecorder.ondataavailable = (event) => {
                        if (event.data.size > 0) {
                            console.log(event.data);
                            this.uploadChunk(event.data);
                        }
                    };

                    for (let track of videoStream.getVideoTracks()) {
                        if (
                            navigator.mediaDevices.getSupportedConstraints()
                                .frameRate
                        ) {
                            track
                                .applyConstraints(this.constraints)
                                .then(() => {})
                                .catch(() => {
                                    console.error("Couldn't set the framerate");
                                });
                        }
                    }

                    if (this.videoRecorder) {
                        this.isRecording = true;
                        this.videoRecorder.start(this.VIDEO_REFRESH);
                    }
                });

            this.realtimeService.connect(this.id);
            this.webSocketSubscription = this.realtimeService
                .getObservable(this.id)
                .subscribe(
                    (msg) => this.receiveData(msg),
                    // Called whenever there is a message from the server
                    (err) => this.receiveError(err),
                    // Called if WebSocket API signals some kind of error
                    () => this.complete()
                    // Called when connection is closed (for whatever reason)
                );
        }
    }

    uploadChunk(chunk) {
        if (chunk && chunk.size > 1) {
            let data64 = this.getBase64Image(this.video);
            let payload = {
                data: data64,
                type: 'upload_chunk',
            };
            this.realtimeService.send(this.id, payload);
        }
    }

    generate_id(length) {
        return [...Array(length)]
            .map((i) => (~~(Math.random() * 36)).toString(36))
            .join('');
    }

    getBase64Image(img: HTMLVideoElement): string {
        var canvas: HTMLCanvasElement = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        let ctx: CanvasRenderingContext2D = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        let dataURL: string = canvas.toDataURL('image/png');
        return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
    }

    receiveData(payload: any) {
        console.log(payload);
    }

    receiveError(err) {
        console.error(err);
        this.stopRecording();
    }

    complete() {}

    stopRecording() {
        this.videoRecorder.requestData();
        if (this.videoRecorder.state !== 'inactive') {
            this.videoRecorder.stop();
            this.showVideo = false;
            this.isRecording = false;

            let payload = {
                data: '',
                type: 'save_recording',
            };
            this.realtimeService.send(this.id, payload);
            this.loadRecord = true;
            setTimeout(() => {
                this.link_Record =
                    'http://127.0.0.1:8000/media/recording_' +
                    this.id +
                    '/my_video.mp4';
                this.showRecord = true;
                this.loadRecord = false;
            }, 3000);
        }
    }
}
