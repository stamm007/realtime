import json
from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import os
import base64
from datetime import datetime
import moviepy.video.io.ImageSequenceClip


class RealTimeConsumer(WebsocketConsumer):
    recording_group_name = None
    recording_id = None

    def connect(self):
        self.recording_group_name = None
        self.recording_id = None

        self.recording_id = self.scope['url_route']['kwargs']['recording_id']
        self.recording_group_name = 'recording_%s' % self.recording_id

        # Join recording group
        async_to_sync(self.channel_layer.group_add)(
            self.recording_group_name,
            self.channel_name
        )

        print(f'Channel name = {self.channel_name}')
     
        try:
          new_folder_images = "./media/"+self.recording_group_name+"/"  
          os.mkdir(new_folder_images) 
        except:
            pass

        self.accept()

    def disconnect(self, _):
        if self.recording_group_name and self.channel_name:
            async_to_sync(self.channel_layer.group_discard)(
                self.recording_group_name,
                self.channel_name
            )
          

    def receive(self, text_data=None, bytes_data=None):
        text_data_json = json.loads(text_data)
        data = text_data_json.get('data', None)
        message_type = text_data_json.get('type', 'upload_chunk')

        async_to_sync(self.channel_layer.group_send)(
            self.recording_group_name,
            {
                'type': message_type,
                'data': data,
            }
        )

    def upload_chunk(self, event):
   
        new_folder_images = "./media/"+self.recording_group_name+"/"
        imgdata = base64.b64decode(event['data'])
        now = datetime.now()
        date_time = now.strftime("%m-%d-%Y-%H-%M-%S")
        filename = new_folder_images+date_time+'.jpg'  
        with open(filename, 'wb') as f:
            f.write(imgdata)   
        f.close()      
        

        pass

    def save_recording(self, event):
        print("save now")
        image_folder = "./media/"+self.recording_group_name
        video_folder = "./media/"+self.recording_group_name
        fps=1
        image_files = [image_folder+'/'+img for img in os.listdir(image_folder) if img.endswith(".jpg")]
        clip = moviepy.video.io.ImageSequenceClip.ImageSequenceClip(image_files, fps=fps)
        clip.write_videofile(video_folder+'/my_video.mp4')
        pass
        


        